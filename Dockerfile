# syntax=docker/dockerfile:1

FROM golang:1.18-alpine AS base
WORKDIR /src
ENV CGO_ENABLED=0
COPY *.go .
COPY go.mod ./
COPY go.sum ./
RUN --mount=type=cache,target=/go/pkg/mod go mod download

FROM base AS build
ARG TARGETOS
ARG TARGETARCH
RUN --mount=target=. \
    --mount=type=cache,target=/go/pkg/mod/ \
    --mount=type=cache,target=/root/.cache/go-build/ \
    GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o /edgecachepreloader .

EXPOSE 5000

ENTRYPOINT ["/edgecachepreloader"]