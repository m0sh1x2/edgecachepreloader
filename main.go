package main

import (
	"gitlab.com/m0sh1x2/edgecachepreloader/routes"
)

func main() {
	// siteToPreload := "everydogmatters.eu"
	// preloadedSites := preload.PreloadAll(siteToPreload)

	// for _, site := range preloadedSites {
	// 	jsonStr, _ := json.Marshal(site)
	// 	log.Println(string(jsonStr))
	// }

	routes.Run()
}
