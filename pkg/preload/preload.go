package preload

import (
	"net/http"
)

// Preload makes several request to specified origin/edge locations

// func PreloadSite(siteAddr string, location string) {
// 	preloadedSite := siteresolve.ResolveSite(siteAddr, location)

// 	log.Println(preloadedSite["Server"])
// 	log.Println("Preloading site on location", siteAddr, location)
// }
type SiteResult struct {
	HttpHeaders http.Header
}

// PreloadAll returns a []SiteResult that contains all of the headers returned from the requested

// func PreloadAll(siteAddr string) []SiteResult {
// 	viper.SetConfigFile("config.yml")
// 	viper.SetConfigType("yaml")
// 	viper.ReadInConfig()

// 	edgeRanges := viper.GetStringSlice("global.config.edgeservers")

// 	// fmt.Println(testConfig)
// 	allResponses := []SiteResult{}

// 	for _, edgeLocation := range edgeRanges {

// 		theSite := siteresolve.ResolveSite(siteAddr, edgeLocation)
// 		allResponses = append(allResponses, SiteResult{
// 			HttpHeaders: theSite,
// 		})

// 		// log.Println("------------------")
// 		log.Println("[Preloading]", theSite["X-Cache-Status"], theSite["X-Edge-Location"], theSite["Cache-Control"], siteAddr, edgeLocation)
// 		time.Sleep(500 * time.Microsecond)
// 	}

// 	return allResponses
// }
