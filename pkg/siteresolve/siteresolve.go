package siteresolve

import (
	"context"
	"log"
	"net"
	"net/http"
	"time"
)

// ResolveSite makes an http request directly to the origin of the site ignoring public DNS records

func ResolveSite(siteAddr, siteOriginIP string) http.Response {
	dialer := &net.Dialer{
		Timeout:   1 * time.Second,
		KeepAlive: 2 * time.Second,

		// DualStack: true, // this is deprecated as of go 1.16
	}
	// or create your own transport, there's an example on godoc.

	// We use .Clone() in order to have the ability to change the siteOriginIP on every call on the custom HTTP client
	tr := http.DefaultTransport.(*http.Transport).Clone()
	tr.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		if addr == siteAddr+":443" {
			addr = siteOriginIP + ":443"
		}
		return dialer.DialContext(ctx, network, addr)
	}

	// client is created in order to use the custom http.DefaultTransport that points to the custom OriginIP
	client := &http.Client{
		Transport: tr,
	}

	resp, err := client.Get("https://" + siteAddr)
	if err != nil {
		log.Println(err)
	}

	// log.Println("[Preloading]", resp.Header["Server"])
	// The response body might be nil in case we have a timeout
	if resp != nil {
		return *resp
	}

	return http.Response{Header: http.Header{}}
}
