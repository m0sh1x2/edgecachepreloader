package routes

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/m0sh1x2/edgecachepreloader/pkg/siteresolve"
	"k8s.io/apimachinery/pkg/util/json"
)

type SiteResponse struct {
	EdgeIP        string `network:"ip4_addr"`
	Server        string
	CacheControl  string
	XCache        string
	XEdgeLocation string
}

func PreloadEndpoint(rg *gin.RouterGroup) {

	rg.GET("/preload/:siteUrl", func(c *gin.Context) {
		siteUrl := c.Param("siteUrl")
		viper.SetConfigFile("config.yml")
		viper.SetConfigType("yaml")
		viper.ReadInConfig()

		edgeRanges := viper.GetStringSlice("global.config.edgeservers")

		w := c.Writer
		header := w.Header()
		header.Set("Transfer-Encoding", "chunked")
		header.Set("Content-Type", "application/json")

		for _, edgeLocation := range edgeRanges {

			theSite := siteresolve.ResolveSite(siteUrl, edgeLocation)
			log.Println(theSite.Header.Get("Server"))
			tempSite, err := json.Marshal(SiteResponse{
				EdgeIP:        edgeLocation,
				XCache:        theSite.Header.Get("X-Cache-Status"),
				XEdgeLocation: theSite.Header.Get("X-Edge-Location"),
				Server:        theSite.Header.Get("Server"),
				CacheControl:  theSite.Header.Get("Cache-Control"),
			})
			if err != nil {
				log.Fatal(err)
			}
			w.Write([]byte(tempSite))
			w.(http.Flusher).Flush()
		}
		w.WriteHeader(http.StatusOK)

	})
}
